﻿[<System.Runtime.CompilerServices.Extension>]
module Arachnid.Polyfills.Kestrel

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Polyfills
open Microsoft.AspNetCore.Http

module Optics =
    /// Provides access to the Kestrel `HttpContext` object for the current request.
    let httpContext_ =
        State.value_<DefaultHttpContext> "Microsoft.AspNetCore.Http.HttpContext"

/// Retreives the Kestrel `HttpContext` object for the current request.
let httpContext = Arachnid.Optic.get Optics.httpContext_

/// Retreives a service held by the Kestrel `IServiceProvider`.
///
/// *Optimization note*: define a static value which pre-applies the type of the
/// service you wish to retreive rather than calling `requestService` from inside
/// of a runtime function.
let requestService<'a> : Arachnid<'a option> =
    httpContext
    |> Arachnid.map (Option.bind (fun x -> x.RequestServices.GetService(typeof<'a>) |> Option.ofObj |> Option.map unbox))

// Polyfill

/// Polyfills for providing support for potential future OWIN standards,
/// allowing Arachnid to provide support for functionality outside of the current
/// standards where the functionality is available in the underlying server in
/// a non-standardised way.

[<RequireQualifiedAccess>]
module Polyfill =

    let private applyPolyfill =
            !. Optics.httpContext_
        >>= function | Some x -> Request.pathRaw_ .= Some (PathAndQuery.path (string x.Request.Path))
                     | _ -> Arachnid.empty

    /// The Arachnid polyfill for Kestrel-based servers. The polyfill should be
    /// included as the first item in a pipeline composition of the Arachnid
    /// application to ensure that any data provided by the polyfill is
    /// available to any subsequent component.
    ///
    /// This polyfill is automatically provided when using the `UseArachnid`
    /// extension method.
    let kestrel =
            applyPolyfill
         *> Pipeline.next

open Microsoft.AspNetCore.Builder

[<System.Runtime.CompilerServices.Extension>]
[<Sealed>]
[<AbstractClass>]
type ApplicationBuilderExtensions private () =

    /// Attaches a Arachnid middleware into an ASP.NET Core application pipeline.
    [<System.Runtime.CompilerServices.Extension>]
    static member UseArachnid(app: IApplicationBuilder, arachnid: Arachnid<PipelineChoice>) =
        let owin : OwinMidFunc = OwinMidFunc.ofArachnid (Polyfill.kestrel *> arachnid)
        app.UseOwin(fun p -> p.Invoke owin)

    /// Builds a Arachnid middleware and attaches it into an ASP.NET Core application pipeline.
    [<System.Runtime.CompilerServices.Extension>]
    static member UseArachnid(app: IApplicationBuilder, arachnidBuider: System.Func<Arachnid<PipelineChoice>>) =
        app.UseArachnid(arachnidBuider.Invoke())

    /// Builds a Arachnid middleware and attaches it into an ASP.NET Core application pipeline.
    [<System.Runtime.CompilerServices.Extension>]
    static member UseArachnid(app: IApplicationBuilder, arachnidBuider: unit -> Arachnid<PipelineChoice>) =
        app.UseArachnid(arachnidBuider())
