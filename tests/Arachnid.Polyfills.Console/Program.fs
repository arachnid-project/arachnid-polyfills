﻿open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Hosting
open Arachnid.Core
open Arachnid.Optics.Http
open Arachnid.Polyfills
open Arachnid.Polyfills.Kestrel
open Hopac

let arachnid =
    arachnid {
        let! requestMethod = Arachnid.Optic.get Request.method_
        let! requestPathBase = Arachnid.Optic.get Request.pathBase_
        let! requestPath = Arachnid.Optic.get Request.pathRaw_
        let! requestQs = Arachnid.Optic.get Request.query_
        do! Arachnid.Optic.set Response.statusCode_ (Some 200)
        let! stream = Arachnid.Optic.get Response.body_
        let sw = new System.IO.StreamWriter(stream)
        do! Arachnid.fromJob <| Job.fromUnitTask (fun () -> sw.WriteLineAsync(sprintf "%A - %A - %A - %A" requestMethod requestPathBase requestPath requestQs))
        do! Arachnid.fromJob <| Job.fromUnitTask (fun () -> sw.WriteLineAsync("Hello world"))
        do! Arachnid.fromJob <| Job.fromUnitTask sw.FlushAsync
        return Halt
    }

type Startup() =
    member __.Configure (app: IApplicationBuilder) =
        app.UseArachnid(arachnid) |> ignore

[<EntryPoint>]
let main args =

    try
        WebHostBuilder()
            .UseKestrel()
            //.UseUrls([| "http://localhost:7000" |])
            //.UseContentRoot(Directory.GetCurrentDirectory())
            //.UseDefaultHostingConfiguration(args)
            .UseStartup<Startup>()
            .Build()
            .Run()
    with
        | ex ->
            printfn "%A" ex

    0
