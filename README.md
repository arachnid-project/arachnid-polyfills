# Arachnid Polyfills

## Overview

Arachnid Polyfills provides adapters to bridge between existing supported OWIN standards and future/proposed OWIN standards, within some of the common web servers available and supported by Arachnid.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-polyfills/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-polyfills/commits/master)

## See Also

For more information see the [meta-repository for the Arachnid Web Stack](https://gitlab.com/arachind-project/arachnid), along with the main [arachnid.io](https://arachnid.io) site.
